from setuptools import setup, find_packages

with open('README.rst', encoding='UTF_8') as f:
    readme = f.read()

setup(
    name='pgbackup',
    version='0.1.0',
    descriptions='Database backups locally or to AWS S3.',
    long_description=readme,
    author='Bill Schenk',
    author_email='bill@schenk.tech',
    install_requires=['boto3'],
    packages=find_packages('src'),
    package_dir={'': 'src'},
    entry_points={
        'console_scripts': [
            'pgbackup=pgbackup.cli:main',
        ]
    }
)
